<?php

namespace Helper;

use \Illuminate\Database\Capsule\Manager as DB;
use \Request;
use \Users;
use \User;
use \Blocktrail\SDK\BlocktrailSDK;
use \DirectReferrals;
use \GeneralSettings;
use \DrLogs;
use \UnilevelLogs;
use \CompanyAllocatedIncome;
use \FlushoutLogs;

class GenericHelper {

    const API_KEY = "ceaf4fed3e04bc298250c25c31837a9dab079392";
    const API_SECRET = "98aa6ee10f7bedcf6f88ad0dfbbd9d7e5c9b964a";
    const IS_TEST_BTC = true;

    public function initBitcoinClient() {
        return new BlocktrailSDK(GenericHelper::API_KEY, GenericHelper::API_SECRET, "BTC", GenericHelper::IS_TEST_BTC);
    }
    
    public function CompanyWallet() {
        $company_wallet = GeneralSettings::where('module_name', '=', 'company_wallet') -> first();
        if ($company_wallet) {
            $c = json_decode($company_wallet -> content);
            $c_company_wallet = $c -> value;
        }
        return (isset($c_company_wallet)) ? $c_company_wallet : "abcdefghijklmnop";
    }

    public function MembershipFee() {
        $code_amount = GeneralSettings::where('module_name', '=', 'codes_amount') -> first();
        if ($code_amount) {
            $c = json_decode($code_amount -> content);
            $c_amount = $c -> value;
        }
        return (isset($c_amount)) ? $c_amount : 0;
    }

    public function sendPaymentBTC($username, $password, $amount, $sentToAddress = null) {
        if (is_null($sentToAddress)) {
            $sentToAddress = GenericHelper::CompanyWallet();
        }

        $arrPayment = array();
        try {
            $client = GenericHelper::initBitcoinClient();

            $wallet = $client -> initWallet($username, $password);
            $value = BlocktrailSDK::toSatoshi($amount);
            $status = $wallet -> pay(array($sentToAddress => $value), false, true, \Blocktrail\SDK\Wallet::BASE_FEE);
        } catch(\Exception $e) {
            // echo $e -> getMessage();
            throw new \Exception($e -> getMessage());
        }
    }

    public function sendMultiPaymentBTC($username, $password, $addresses) {
        $arrPayment = array();
        try {
            $client = GenericHelper::initBitcoinClient();

            $wallet = $client -> initWallet($username, $password);
            $status = $wallet -> pay($addresses, false, true, \Blocktrail\SDK\Wallet::BASE_FEE);
        } catch(\Exception $e) {
            // echo $e -> getMessage();
            throw new \Exception($e -> getMessage());
        }
    }

    public function createNewWallet($username, $password) {
        $Arrwallet = array();
        try {
            $client = GenericHelper::initBitcoinClient();

            list($wallet, $primaryMnemonic, $backupMnemonic, $blocktrailPublicKeys) = $client -> createNewWallet($username, $password);
            $address = $wallet -> getNewAddress();

            $Arrwallet['status'] = "Success";
            $Arrwallet['address'] = $address;
            $Arrwallet['username'] = $username;
            $Arrwallet['password'] = $password;
            $Arrwallet['primary_mnemonic'] = $primaryMnemonic;
            $Arrwallet['backup_mnemonic'] = $backupMnemonic;
            $Arrwallet['blocktrail_public_keys'] = $blocktrailPublicKeys;
        } catch(\Exception $e) {
            // echo $e -> getMessage();
            $Arrwallet['status'] = "Error";
            $Arrwallet['message'] = $e -> getMessage();
        }
        return $Arrwallet;
    }

    public function generateNewAddress($username, $password) {
        $Arrwallet = array();
        try {
            $client = GenericHelper::initBitcoinClient();
            $wallet = $client -> initWallet($username, $password);
            $address = $wallet -> getNewAddress();
            $Arrwallet['address'] = $address;
        } catch(\Exception $e) {
            // echo $e -> getMessage();
            $Arrwallet['status'] = "Error";
            $Arrwallet['message'] = $e -> getMessage();
        }
        return $Arrwallet;
    }

    public function setWalletWebHook($wallet_address, $user, $abc = "") {
        $client = GenericHelper::initBitcoinClient();
        $url = sprintf("%swebhookHandler/%s", GenericHelper::baseUrl(), $wallet_address);
        $client -> setupWebhook($url, $user -> username . $abc);
        $client -> subscribeAddressTransactions($user -> username . $abc, $wallet_address, 1);
    }

    public function getWalletBalance($username, $password) {
        $arrBalance = array();
        try {
            $client = GenericHelper::initBitcoinClient();

            $wallet = $client -> initWallet($username, $password);
            list($confirmedBalance, $unconfirmedBalance) = $wallet -> getBalance();

            $arrBalance['status'] = "Success";
            $arrBalance['confirmed_balance'] = BlocktrailSDK::toBTC($confirmedBalance);
            $arrBalance['unconfirmed_balance'] = BlocktrailSDK::toBTC($unconfirmedBalance);
        } catch(\Exception $e) {
            // echo $e -> getMessage();
            $arrBalance['status'] = "Error";
            $arrBalance['message'] = $e -> getMessage();
        }
        return $arrBalance;
    }

    public function getAddressInfo($address) {
        $arrAddress = array();
        try {
            $client = GenericHelper::initBitcoinClient();

            $client_address = $client -> address($address);

            $arrAddress['status'] = "Success";
            $arrAddress['address'] = $client_address;
        } catch(\Exception $e) {
            // echo $e -> getMessage();
            $arrAddress['status'] = "Error";
            $arrAddress['message'] = $e -> getMessage();
        }
        return $arrAddress;
    }

    public function getClient() {
        return GenericHelper::initBitcoinClient();
    }

    public function getWalletTx($username, $password) {
        $arrTX = array();
        try {
            $client = GenericHelper::initBitcoinClient();

            $wallet = $client -> initWallet($username, $password);
            $transactions = $wallet -> transactions();

            $arrTX['status'] = "Success";
            $arrTX['transactions'] = $transactions;
        } catch(\Exception $e) {
            // echo $e -> getMessage();
            $arrTX['status'] = "Error";
            $arrTX['message'] = $e -> getMessage();
        }
        return $arrTX;
    }

    public function convertBTCtoUSD($btc) {
        $arrBTC = array();
        try {
            $btcRate = GenericHelper::convertUSDtoBTC(1);
            $usd = $btc / $btcRate;

            $arrBTC['status'] = "Success";
            $arrBTC['value'] = $usd;
        } catch(\Exception $e) {
            // echo $e -> getMessage();
            $arrBTC['status'] = "Error";
            $arrBTC['message'] = $e -> getMessage();
            $arrBTC['value'] = 0;
        }
        return $arrBTC;
    }

    public function convertUSDtoBTC($amount) {
        try {
            // $url = sprintf("https://blockchain.info/tobtc?currency=USD&value=%s", $amount);
            // $d = file_get_contents($url);

            $bitcoin_rates = file_get_contents(BITCOIN_RATES_FILE_PATH);
            $bitcoin_rates = json_decode($bitcoin_rates);

            $usd = $bitcoin_rates -> BTC_USD;
            $convertedBTC = $usd * $amount;
            // $convertedBTC = number_format($usd, 8) * $amount;
            // $convertedBTC = number_format($convertedBTC, 8);

            return $convertedBTC;
        } catch(\Exception $e) {
            return 0;
        }
    }

    public function convertToBTC($currency, $amount) {
        try {
            // $url = "https://blockchain.info/tobtc?currency=" . $currency . "&value=" . $amount;
            // $d = file_get_contents($url);

            $bitcoin_rates = file_get_contents(BITCOIN_RATES_FILE_PATH);
            $bitcoin_rates = json_decode($bitcoin_rates, TRUE);

            $curr = sprintf("BTC_%s", strtoupper($currency));

            $a = $bitcoin_rates[$curr];
            $convertedBTC = $a * $amount;
            // $convertedBTC = number_format($a, 8) * $amount;
            // $convertedBTC = number_format($convertedBTC, 8);

            return $convertedBTC;
        } catch(\Exception $e) {
            return 0;
        }
    }

    public function decodeSatoshi($satoshi_value) {
        return BlocktrailSDK::toBTC($satoshi_value);
    }

    public function encodeSatoshi($btc) {
        return BlocktrailSDK::toSatoshi($btc);
    }
    
    public function getWalletCredentials() {
        // wallet_address
        $wallet_address = GeneralSettings::where('module_name', '=', 'wallet_address') -> first();
        if ($wallet_address) {
            $a = json_decode($wallet_address -> content);
            $u['wallet_address'] = $a -> value;
        }

        // wallet_username
        $wallet_username = GeneralSettings::where('module_name', '=', 'wallet_username') -> first();
        if ($wallet_username) {
            $b = json_decode($wallet_username -> content);
            $u['wallet_username'] = $b -> value;
        }

        // wallet_password
        $wallet_password = GeneralSettings::where('module_name', '=', 'wallet_password') -> first();
        if ($wallet_password) {
            $c = json_decode($wallet_password -> content);
            $u['wallet_password'] = $c -> value;
        }
        return $u;
    }

    public function timeAgo($time_ago) {
        $time_ago = strtotime($time_ago);
        $cur_time = time();
        $time_elapsed = $cur_time - $time_ago;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);
        // Seconds
        if ($seconds <= 60) {
            return "just now";
        }
        //Minutes
        else if ($minutes <= 60) {
            if ($minutes == 1) {
                return "one minute ago";
            } else {
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if ($hours <= 24) {
            if ($hours == 1) {
                return "an hour ago";
            } else {
                return "$hours hrs ago";
            }
        }
        //Days
        else if ($days <= 7) {
            if ($days == 1) {
                return "yesterday";
            } else {
                return "$days days ago";
            }
        }
        //Weeks
        else if ($weeks <= 4.3) {
            if ($weeks == 1) {
                return "a week ago";
            } else {
                return "$weeks weeks ago";
            }
        }
        //Months
        else if ($months <= 12) {
            if ($months == 1) {
                return "a month ago";
            } else {
                return "$months months ago";
            }
        }
        //Years
        else {
            if ($years == 1) {
                return "one year ago";
            } else {
                return "$years years ago";
            }
        }
    }

    public function sendMail($sendTo, $subject, $contentBody = "") {
        try {
            $_username = 'support@upnext.shop';

            $mail = new \PHPMailer;

            $mail -> SMTPDebug = 0;

            $mail -> isSMTP();
            $mail -> Host = 'smtp.gmail.com';
            $mail -> SMTPAuth = true;
            $mail -> Username = 'upnexttrading@gmail.com';
            $mail -> Password = 'upnext-trading';
            $mail -> SMTPSecure = 'ssl';
            $mail -> Port = 465;

            $mail -> setFrom($_username, '[UpNext - Trading] - No reply');
            $mail -> addAddress($sendTo);
            // $mail -> addAddress('ellen@example.com');
            // $mail -> addReplyTo('info@example.com', 'Information');
            // $mail -> addCC('cc@example.com');
            // $mail -> addBCC('bcc@example.com');
            // $mail -> addAttachment('/var/tmp/file.tar.gz');
            $mail -> isHTML(true);

            $mail -> Subject = $subject;
            $mail -> Body = $contentBody;
            // $mail -> AltBody = 'This is the body in plain text for non-HTML mail clients';
            if (!$mail -> send()) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mail -> ErrorInfo;
                return 'Mailer Error: ' . $mail -> ErrorInfo;
            } else {
                echo 'Message has been sent';
                return true;
            }
        } catch(\Exception $e) {
            echo $e -> getMessage();
            return 'Mailer Error: ' . $e -> getMessage();
        }
    }

    /*
     * Email Notification
     */
    public function sendMailInquiry($name, $fromEmail, $subject, $contentBody = "") {
        $_username = 'support@upnext.shop';

        $mail = new \PHPMailer;

        $mail -> SMTPDebug = 0;

        $mail -> isSMTP();
        $mail -> Host = 'smtp.gmail.com';
        $mail -> SMTPAuth = true;
        $mail -> Username = 'upnexttrading@gmail.com';
        $mail -> Password = 'upnext-trading';
        $mail -> SMTPSecure = 'ssl';
        $mail -> Port = 465;

        $mail -> setFrom($_username, "Support/Inquiry");
        $mail -> addAddress($_username);
        // $mail -> addAddress('ellen@example.com');
        $mail -> addReplyTo($fromEmail, $name);
        // $mail -> addCC('cc@example.com');
        // $mail -> addBCC('bcc@example.com');

        // $mail -> addAttachment('/var/tmp/file.tar.gz');
        // $mail -> addAttachment('/tmp/image.jpg', 'new.jpg');
        $mail -> isHTML(true);

        $mail -> Subject = $subject;
        $mail -> Body = $contentBody;
        // $mail -> AltBody = 'This is the body in plain text for non-HTML mail clients';

        if (!$mail -> send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail -> ErrorInfo;
            return false;
        } else {
            echo 'Message has been sent';
            return true;
        }
    }

    protected function baseUrl() {
        $path = dirname($_SERVER['SCRIPT_NAME']);
        $path = trim($path, '/');
        $baseUrl = Request::getUrl();
        $baseUrl = trim($baseUrl, '/');
        return $baseUrl . '/' . $path . ($path ? '/' : '');
    }

    public function sendAccountVerification($user, $activationCode, $password) {
        $to = $user -> email;
        $subject = "[UpNext - Trading] Account Verification";
        $body = "<p style='color: blue; font-weight: bold;'><b>Your have successfully Registered to UpNext Trading.</b><p>";
        $body .= "<p>";
        $body .= "<span>Username</span>: <b>" . $user -> username . "</b>";
        $body .= "</p>";
        $body .= "<p>";
        $body .= "<span>Password</span>: <b>" . $password . "</b>";
        $body .= "</p>";
        $body .= "<p>click here to verify your account </br>" . "<b>" . sprintf('%sregistration/confirmation/%s/%s', GenericHelper::baseUrl(), $user -> id, $activationCode) . "</b></p>";
        // return GenericHelper::sendMail($to, $subject, $body);
        
        $mail_cmd = sprintf("php %s/mail_notification.php %s %s %s &", WORKERS_PATH, $to, base64_encode($subject), base64_encode($body));
        pclose(popen($mail_cmd, "w"));
        
        return true;
    }

    public function resendEmailVerification($user) {
        $to = $user -> email;

        $subject = "[UpNext - Trading] Account Verification";
        $body = "<p style='color: blue; font-weight: bold;'><b>Your have successfully Registered to UpNext Trading.</b><p>";
        $body .= "<p>";
        $body .= "<span>Username</span>: <b>" . $user -> username . "</b>";
        $body .= "</p>";
        $body .= "<p>";
        $body .= "<span>Password</span>: <b>" . base64_decode($user -> canonical_hash) . "</b>";
        $body .= "</p>";
        $body .= "<p>click here to verify your account </br>" . "<b>" . sprintf('%sregistration/confirmation/%s/%s', GenericHelper::baseUrl(), $user -> id, $user -> activation_code) . "</b></p>";
        // return GenericHelper::sendMail($to, $subject, $body);
        
        $mail_cmd = sprintf("php %s/mail_notification.php %s %s %s &", WORKERS_PATH, $to, base64_encode($subject), base64_encode($body));
        pclose(popen($mail_cmd, "w"));
        
        return true;
    }

}
