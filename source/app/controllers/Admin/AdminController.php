<?php

namespace Admin;

use \App;
use \View;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;

class AdminController extends BaseController {
		
	/**
	 * display the admin dashboard
	 */
	public function index() {
	    Response::Redirect($this -> siteUrl("admin/codes"));
	}

}
