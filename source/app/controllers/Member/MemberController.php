<?php

namespace Member;

use \App;
use \View;
use \Input;
use \Sentry;
use \Menu;
use \Response;

class MemberController extends BaseController {

	/**
	 * display the member dashboard
	 */
	public function index() {
		$user = Sentry::getUser();

		if ($user -> is_registered == 1) {
			Response::Redirect($this -> siteUrl(sprintf('member/geneology/%s', $user -> id)));
		} else {
			Response::Redirect($this -> siteUrl("member/purchase/activation_code"));
		}
	}

}
