<?php

namespace Member;

use \App;
use \Menu;
use \Module;
use \Sentry;

use \Bulletin;

class BaseController extends \BaseController {
	public function __construct() {
		parent::__construct();
		$this -> data['menu_pointer'] = '<div class="pointer"><div class="arrow"></div><div class="arrow_border"></div></div>';

		$adminMenu = Menu::create('member_sidebar');
		// $dashboard = $adminMenu->createItem ( 'dashboard', array (
		// 'label' => 'Genealogy',
		// 'icon' => 'sitemap',
		// 'url' => 'member'
		// ) );
		//
		// $user = Sentry::getUser();
		// $DR = MembersProfiles::join('direct_referrals as dr','dr.profile_id', '=', 'members_profile.id')
		// ->leftJoin('users as u', function($join) {
		// $join->on('u.id', '=', 'dr.recruitee_id');
		// })
		// ->where('members_profile.user_id', '=', $user->id)
		// ->select(['*', 'u.username as uName'])
		// ->get()
		// ->toArray();
		//
		// if (count($DR) > 0) {
		// $adminMenu->addItem ( 'dashboard', $dashboard );
		// $adminMenu->setActiveMenu ( 'dashboard' );
		// }

		foreach (Module::getModules () as $module) {
			$module -> registerMemberMenu();
		}
		
		$this -> data['bulletins'] = Bulletin::all();
	}

}
