<?php

namespace Geneology;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'Geneology';
	}

	public function getModuleAccessor() {
		return 'geneology';
	}

	// public function registerMemberMenu(){
	// }

	public function registerPublicRoute() {
		Route::resource('/geneology/:user_id', 'Geneology\Controllers\GeneologyController');
		
		Route::get('/register/geneology', 'Geneology\Controllers\GeneologyController:accountRegistration');
		
		Route::post('/register/member/geneology', 'Geneology\Controllers\GeneologyController:registerMemberOnGeneology') -> name("register_geneology");
		Route::post('/register/member/geneology/new', 'Geneology\Controllers\GeneologyController:addNewMember') -> name("addNewMember");
	}

}
