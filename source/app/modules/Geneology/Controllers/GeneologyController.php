<?php

namespace Geneology\Controllers;

use \User;
use \Users;
use \HierarchySiblings;
use \UsersHasHierarchySiblings;
use \DirectReferrals;
use \Codes;
use \UserBalance;
use \ExitHistory;
use \CodeGenerator;
use \BonusManager;
use \CrosslineValidator;
use \GeneralSettings;
use \GenericHelper;

use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Member\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

class GeneologyController extends BaseController {
	public function __construct() {
		parent::__construct ();
		$this->data ['active_menu'] = 'geneology';
	}
	
	/**
	 * display the admin dashboard
	 */
	public function index($user_id = 1) {
		$user = Sentry::getUser ();
		
		// $placement = BonusManager::getPosition(2);
		// print_r(json_encode($placement));exit;
		
		$this->data ['title'] = 'Genealogy';
		// set user id when item cliked
		$id = ($user_id == 1) ? $user->id : $user_id;
		$this->data ['dynamic_user'] = Sentry::findUserById ( $id );
		
		$hasExitRecordTable1 = ExitHistory::where ( 'user_id', '=', $id )->where ( 'table', '=', 1 )->first ();
		$table = ($hasExitRecordTable1) ? 2 : 1;
		
		$this->data ['table'] = $table;
		$this->data ['user_id'] = $id;
		
		App::render ( '@geneology/index.twig', $this->data );
	}
	public function accountRegistration() {
		$user = Sentry::getUser ();
		
		$id = ($user->is_registered == 1) ? $user->id : 6;
		$this->data ['dynamic_user'] = $id;
		
		$this->data ['title'] = 'Geneology Registration';
		App::render ( '@geneology/account_activation.twig', $this->data );
	}
	public function registerMemberOnGeneology() {
		$position = Input::post ( "position" );
		$head_ref_code = Input::post ( "head_ref_code" );
		$activation_code = Input::post ( "activation_code" );
		$direct_referral = Input::post ( "direct_referral" );
		
		try {
			$user = Sentry::getUser ();
			
			$pass_code = Codes::where ( "generated_code", "=", $activation_code )
			                 ->where ( "is_used", "=", 0 )
			                 ->first ();
			if ($pass_code) {
				$dr_id = 6;
				if (! is_null ( $direct_referral )) {
					$direct = Users::where ( "username", "=", $direct_referral )->first ();
					if ($direct) {
						$dr_id = $direct->id;
					}
				}
				$DR = Sentry::findUserById ( $dr_id );
				$head = Users::where ( "ref_code", "=", $head_ref_code )->first ();
				if ($head) {
					$hierarchy = new HierarchySiblings ();
					$hierarchy->recruitee_id = $user->id;
					$hierarchy->position = (strcasecmp ( $position, "Left" ) == 0) ? 0 : 1;
					
					if ($hierarchy->save ()) {
						$hasSiblings = new UsersHasHierarchySiblings ();
						$hasSiblings->user_id = $head->id;
						$hasSiblings->hierarchy_sibling_id = $hierarchy->id;
						$hasSiblings->table = 1;
						$hasSiblings->save ( array (
								"timestamps" => false 
						) );
						
						// set direct referral
                        $dr_amount = 100;
                        $dr_bonus = GeneralSettings::where('module_name', '=', 'dr_bonus') -> first();
                        if ($dr_bonus) {
                            $c = json_decode($dr_bonus -> content);
                            $dr_amount = $c -> value;
                        }
						$referral = new DirectReferrals ();
						$referral->recruiter_id = $DR->id;
						$referral->recruitee_id = $user->id;
						$referral->dr_earnings = $dr_amount;
						$referral->save ();
						
						$pass_code->user_id = $user->id;
						$pass_code->is_used = 1;
						$pass_code->save ();
						
						$user->is_registered = 1;
						$user->save ();
						
						BonusManager::processExitBonus ( 1, 8000 );
					}
				} else {
					App::flash ( 'message_status', false );
					App::flash ( 'message', 'Invalid Head on Geneology table.' );
				}
			} else {
				App::flash ( 'message_status', false );
				App::flash ( 'message', 'Code already in used.' );
			}
		} catch ( Cartalyst\Sentry\Users\UserNotFoundException $e ) {
			App::flash ( 'message_status', false );
			App::flash ( 'message', 'User was not found.' );
		}
		Response::redirect ( $this->siteUrl ( "member" ) );
	}
	
	public function addNewMember() {
	    $curr_user = Sentry::getUser();
	    
	    $codeGen = new CodeGenerator();
        
        $username = Input::post("username");
        $full_name = Input::post("full_name");
        $email = Input::post("email");
        $activation_code = Input::post("activation_code");
        $direct_referral = Input::post("direct_referral");
        $position = Input::post ( "position" );
        $head_ref_code = Input::post ( "head_ref_code" );
        
        // try {
            $pass_code = Codes::where ( "generated_code", "=", $activation_code )
                             ->where ( "is_used", "=", 0 )
                             ->first ();
            if ($pass_code) {
                $dr_id = 6;
                if (! is_null ( $direct_referral )) {
                    $direct = Users::where ( "username", "=", $direct_referral )->first ();
                    if ($direct) {
                        $dr_id = $direct->id;
                    }
                }
                
                $DR = Sentry::findUserById ( $dr_id );
                $head = Users::where ( "ref_code", "=", $head_ref_code )->first ();
                if ($head) {
                    $randomPassword = $codeGen -> getToken(12);
                    $credential =  ['ref_code' => $codeGen -> getReferenceCode(), 
                                    'username' => trim($username), 
                                    'email' => trim($email), 
                                    'full_name' => trim($full_name), 
                                    'password' => $randomPassword, 
                                    'canonical_hash' => base64_encode($randomPassword),
                                    'activated' => 0, 
                                    'is_registered' => 1, 
                                    'permissions' => array('member' => 1),
                                    'user_type' => 2
                                   ];
                    $user = Sentry::createUser($credential);
                    $user -> addGroup(Sentry::getGroupProvider() -> findByName('Members'));
                    
                    $hierarchy = new HierarchySiblings ();
                    $hierarchy->recruitee_id = $user->id;
                    $hierarchy->position = (strcasecmp ( $position, "Left" ) == 0) ? 0 : 1;
                    
                    if ($hierarchy->save ()) {
                        $hasSiblings = new UsersHasHierarchySiblings ();
                        $hasSiblings->user_id = $head->id;
                        $hasSiblings->hierarchy_sibling_id = $hierarchy->id;
                        $hasSiblings->table = 1;
                        $hasSiblings->save ( array (
                                "timestamps" => false 
                        ) );
                        
                        // set direct referral
                        $dr_amount = 100;
                        $dr_bonus = GeneralSettings::where('module_name', '=', 'dr_bonus') -> first();
                        if ($dr_bonus) {
                            $c = json_decode($dr_bonus -> content);
                            $dr_amount = $c -> value;
                        }
                        $referral = new DirectReferrals ();
                        $referral->recruiter_id = $DR->id;
                        $referral->recruitee_id = $user->id;
                        $referral->dr_earnings = $dr_amount;
                        $referral->save ();
                        
                        $pass_code->user_id = $user->id;
                        $pass_code->is_used = 1;
                        $pass_code->save ();
                        
                        BonusManager::processExitBonus ( 1, 8000 );
                        
                        $activationCode = $user -> getActivationCode();
                        // $sendVerification = GenericHelper::sendAccountVerification($user, $activationCode, $randomPassword);
                    }
                } else {
                    App::flash ( 'message_status', false );
                    App::flash ( 'message', 'Invalid Head on Geneology table.' );
                }
            } else {
                App::flash ( 'message_status', false );
                App::flash ( 'message', 'Code already in used.' );
            }
        // } catch ( Cartalyst\Sentry\Users\UserNotFoundException $e ) {
            // App::flash('message_status', false);
            // App::flash('message', "Sponsor must exist! Please enter a valid sponsor username.");
        // } catch ( \Exception $e ) {
            // App::flash('message_status', false);
            // App::flash('message', "[Exception:]" . $e -> getMessage());
        // }
        Response::redirect($this -> siteUrl('/member/geneology/' . $curr_user -> id));
	}
}
