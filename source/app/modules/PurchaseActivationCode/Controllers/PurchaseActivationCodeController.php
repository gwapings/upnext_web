<?php

namespace PurchaseActivationCode\Controllers;

use \User;
use \Users;
use \App;
use \View;
use \Menu;
use \Input;
use \Sentry;
use \Request;
use \Response;
use \Exception;
use \Member\BaseController;
use \Cartalyst\Sentry\Users\UserNotFoundException;

use \GenericHelper;
use \GeneralSettings;
use \PurchaseCode;
use \PurchaseCodeProof;
use \CodeGenerator;
use \Codes;

class PurchaseActivationCodeController extends BaseController {

	public function __construct() {
		parent::__construct();
		$this -> data['active_menu'] = 'purchase_code';
	}

	/**
	 * display the admin dashboard
	 */
	public function index() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Purchase Activation Code';
		$this -> data['membership_fee'] = GenericHelper::MembershipFee();
		$this -> data['is_registered'] = ($user -> is_registered == 1) ? TRUE : FALSE;

		App::render('@purchaseactivationcode/index.twig', $this -> data);
	}

	public function viewPurchaseHistory() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Purchase History';

		$purchase_codes = PurchaseCode::where("user_id", "=", $user -> id) -> get();
		$this -> data['purchase_codes'] = $purchase_codes;

		App::render('@purchaseactivationcode/purchase_history.twig', $this -> data);
	}

	public function viewProofOfPayment($purchase_id) {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Purchase Proof Documents';

		$proofs = PurchaseCodeProof::where("purchase_code_id", "=", $purchase_id) -> get();
		$this -> data['purchase_proofs'] = $proofs;

		App::render('@purchaseactivationcode/purchase_proofs.twig', $this -> data);
	}

	public function viewIssuedCodes() {
		$user = Sentry::getUser();
		$this -> data['title'] = 'Issued Codes';
		
		$unused_codes = Codes::leftJoin("purchase_code","purchase_code.id","=","codes.purchase_code_id")
		                      -> where("purchase_code.user_id", "=", $user -> id) 
		                      -> where("codes.is_used", "=", 0) 
		                      -> select("*", "codes.id", "codes.created_at")
		                      -> get();
		$this -> data['unused_codes'] = $unused_codes;
		
		$used_codes = Codes::leftJoin("purchase_code","purchase_code.id","=","codes.purchase_code_id")
                              -> where("purchase_code.user_id", "=", $user -> id) 
                              -> where("codes.is_used", "=", 1) 
                              -> select("*", "codes.id", "codes.created_at")
                              -> get();
		$this -> data['used_codes'] = $used_codes;
		
		$this -> data['is_registered'] = ($user -> is_registered == 1) ? true : false;

		App::render('@purchaseactivationcode/purchase_codes.twig', $this -> data);
	}
	
	public function cashPayment() {
		$user = Sentry::getUser();
		$codeGen = new CodeGenerator();

		$quantity = Input::post("quantity");
		$remarks = Input::post("remarks");

		// code amount
		$c_amount = 0;
		$code_amount = GeneralSettings::where('module_name', '=', 'codes_amount') -> first();
		if ($code_amount) {
			$c = json_decode($code_amount -> content);
			$c_amount = $c -> value;
		}
		$totalAmount = $c_amount * $quantity;

		$purchase_code = new PurchaseCode();
		$purchase_code -> reference_code = $codeGen -> getToken();
		$purchase_code -> user_id = $user -> id;
		$purchase_code -> total_amount = $totalAmount;
		$purchase_code -> payment_method = 1;
		$purchase_code -> quantity = $quantity;
		$purchase_code -> code_amount = $c_amount;
		$purchase_code -> remarks = $remarks;
		$purchase_code -> save();

		$payment_proofs = Input::file("payment_proofs");
		$payment_proofs_length = count($payment_proofs["name"]);
		for ($i = 0; $i < $payment_proofs_length; $i++) {
			$filename = $payment_proofs["name"][$i];
			$tmpName = $payment_proofs["tmp_name"][$i];
			$type = $payment_proofs["type"][$i];

			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			$allowed = array('png', 'PNG', 'jpg', 'JPG');
			if (!in_array($ext, $allowed)) {
				App::flash('message_status', false);
				App::flash('message', 'Invalid File Type');
				Response::redirect($this -> siteUrl('member/purchase/activation_code'));
				return;
			}

			$payment_proof_dir = "assets/images/purchase_proof/" . $user -> id;
			if (!file_exists($payment_proof_dir)) {
				mkdir($payment_proof_dir, 0777, TRUE);
			}

			$path = sprintf("%s/%s.%s", $payment_proof_dir, md5($filename), $ext);

			if (move_uploaded_file($tmpName, $path)) {
				$purchaseCodeProof = new PurchaseCodeProof();
				$purchaseCodeProof -> purchase_code_id = $purchase_code -> id;
				$purchaseCodeProof -> file_name = $path;
				$purchaseCodeProof -> save();
			}
		}

		App::flash('message_status', true);
		App::flash('message', 'Transaction Completed.');
		Response::redirect($this -> siteUrl('member/purchase/activation_code'));
	}

}
