<?php

namespace PurchaseActivationCode;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'PurchaseActivationCode';
	}

	public function getModuleAccessor() {
		return 'purchaseactivationcode';
	}

	// public function registerMemberMenu(){
	// }

	public function registerPublicRoute() {
		Route::resource('/purchase/activation_code', 'PurchaseActivationCode\Controllers\PurchaseActivationCodeController');
		
		Route::get('/purchase/issued_code/view', 'PurchaseActivationCode\Controllers\PurchaseActivationCodeController:viewIssuedCodes');
		Route::get('/purchase/history/view', 'PurchaseActivationCode\Controllers\PurchaseActivationCodeController:viewPurchaseHistory');
		Route::get('/purchase/:purchase_id/proofs', 'PurchaseActivationCode\Controllers\PurchaseActivationCodeController:viewProofOfPayment');
		
		Route::post('/purchase/payment/cash', 'PurchaseActivationCode\Controllers\PurchaseActivationCodeController:cashPayment') -> name('cash_payment');
	}

}
