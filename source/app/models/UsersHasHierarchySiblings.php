<?php
use Illuminate\Database\Eloquent\Model as Model;

class UsersHasHierarchySiblings extends Model {
	public function user($id, $table) {
    	return UsersHasHierarchySiblings::where('users_has_hierarchy_siblings.user_id', '=', $id)
    				->where('users_has_hierarchy_siblings.table', '=', $table)
    				->select('*');

    }

    public function siblings($id, $table) {
        return UsersHasHierarchySiblings::join('hierarchy_siblings as h', 'h.id', "=", "users_has_hierarchy_siblings.hierarchy_sibling_id")
                    ->where('users_has_hierarchy_siblings.user_id', '=', $id)
                    ->where('users_has_hierarchy_siblings.table', '=', $table)
                    ->select(array('recruitee_id','position'));
    }
    
    public function hasLeft($id) {
    	$siblings = UsersHasHierarchySiblings::siblings($id, $table)->get();
    	foreach ($siblings as $key => $sib) {
			if ($sib -> position === 0) {
				return true;
			}
		}
		return false;
    }
    
    public function hasRight($id) {
    	$siblings = UsersHasHierarchySiblings::siblings($id, $table)->get();
    	foreach ($siblings as $key => $sib) {
			if ($sib -> position === 1) {
				return true;
			}
		}
		return false;
    }
    
    public function hasEntry($userid, $table) {
    	$heirarchy = UsersHasHierarchySiblings::leftJoin('hierarchy_siblings as HS', 'HS.id','=','users_has_hierarchy_siblings.hierarchy_sibling_id')
						-> where('HS.recruitee_id','=',$userid)
						-> where('users_has_hierarchy_siblings.table','=',$table)
						-> select(array('*'))
						-> get();
    	if (count($heirarchy) > 0) {
    		$has_entry = true;
    	} else {
    		$has_entry = false;
    	}
    	return $has_entry;
    }
}

// SELECT * FROM `users_has_hierarchy_siblings` 
// JOIN `hierarchy_siblings` as `h` ON `h`.`id` = `users_has_hierarchy_siblings`.`hierarchy_sibling_id`  
// WHERE `users_has_hierarchy_siblings`.`user_id` = 15
