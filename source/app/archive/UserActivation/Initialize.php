<?php

namespace UserActivation;

use \MembersProfiles;
use \DirectReferrals;

use \App;
use \Menu;
use \Route;
use \Sentry;

class Initialize extends \SlimStarter\Module\Initializer{

    public function getModuleName(){
        return 'UserActivation';
    }

    public function getModuleAccessor(){
        return 'useractivation';
    }

    public function registerMemberMenu(){

        $adminMenu = Menu::get('member_sidebar');

        $member = $adminMenu->createItem('2useractivation', array(
            'label' => 'Account Activation',
            'icon'  => 'power-off',
            'url'   => 'member/useractivation'
        ));

        $user = Sentry::getUser();
      	if (!DirectReferrals::hasRegistered($user -> id)) {
        	$adminMenu->addItem('2useractivation', $member);
        }
    }
    
    
    public function registerPublicRoute(){
        Route::resource('/useractivation', 'UserActivation\Controllers\UserActivationController');
        Route::post('/user/account/activation', 'UserActivation\Controllers\UserActivationController:accountActivationRegister')->name('user_account_activation');
    }
}
