<?php

namespace GGAdminLoad;

use \MembersProfiles;
use \DirectReferrals;

use \App;
use \Menu;
use \Route;


class Initialize extends \SlimStarter\Module\Initializer{

    public function getModuleName(){
        return 'GGAdminLoad';
    }

    public function getModuleAccessor(){
        return 'adminload';
    }

    public function registerAdminMenu(){

        $adminMenu = Menu::get('admin_sidebar');

        $member = $adminMenu->createItem('adminload', array(
            'label' => 'Admin Power 7 Booster Loading',
            'icon'  => 'mobile-phone',
            'url'   => 'admin/adminload'
        ));

    	$adminMenu->addItem('adminload', $member);
    }
    
    
    public function registerAdminRoute(){
        Route::resource('/adminload', 'GGAdminLoad\Controllers\AdminLoadController');
    }
}
