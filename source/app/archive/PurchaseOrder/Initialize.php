<?php

namespace PurchaseOrder;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer {

	public function getModuleName() {
		return 'PurchaseOrder';
	}

	public function getModuleAccessor() {
		return 'purchaseorder';
	}

	//     public function registerMemberMenu(){
	//         $adminMenu = Menu::get('member_sidebar');
	//         $member = $adminMenu->createItem('purchaseorder', array(
	//             'label' => 'Purchase Code',
	//             'icon'  => 'shopping-cart',
	//             'url'   => 'member/purchaseorder'
	//         ));
	//     	$adminMenu->addItem('purchaseorder', $member);
	//     }

	public function registerPublicRoute() {
		Route::resource('/purchaseorder', 'PurchaseOrder\Controllers\PurchaseOrderController');
		Route::post('/purchaseorder/doOrderRequest', 'PurchaseOrder\Controllers\PurchaseOrderController:doOrderRequest') -> name('upload_image');
	}

}
