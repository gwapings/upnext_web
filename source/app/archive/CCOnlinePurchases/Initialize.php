<?php

namespace CCOnlinePurchases;

use \App;
use \Menu;
use \Route;

class Initialize extends \SlimStarter\Module\Initializer{

    public function getModuleName(){
        return 'CCOnlinePurchases';
    }

    public function getModuleAccessor(){
        return 'onlinepurchases';
    }

    public function registerAdminMenu(){

        $adminMenu = Menu::get('admin_sidebar');

        $member = $adminMenu->createItem('onlinepurchases', array(
            'label' => 'Online Purchases',
            'icon'  => 'shopping-cart',
            'url'   => 'admin/onlinepurchases'
        ));

    	$adminMenu->addItem('onlinepurchases', $member);
    }

    public function registerAdminRoute(){
        Route::resource('/onlinepurchases', 'CCOnlinePurchases\Controllers\OnlinePurchasesController');
        Route::post('/onlinepurchases/reviewTransaction', 'CCOnlinePurchases\Controllers\OnlinePurchasesController:reviewTransaction');
    }
}
