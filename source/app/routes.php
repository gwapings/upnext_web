<?php

/**
 * Sample group routing with user check in middleware
 */
/**
 * Sample group routing with user check in middleware
 */
Route::group('/admin', function() {
    if (!Sentry::check()) {
        if (Request::isAjax()) {
            Response::headers() -> set('Content-Type', 'application/json');
            Response::setBody(json_encode(array('success' => false, 'message' => 'Session expired or unauthorized access.', 'code' => 401)));
            App::stop();
        } else {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    } else {
        $user = Sentry::getUser();
        if (!$user -> hasAnyAccess(array('admin'))) {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    }
}, function() use ($app) {
    /** sample namespaced controller */
    Route::get('/', 'Admin\AdminController:index') -> name('admin');
    foreach (Module::getModules() as $module) {
        $module -> registerAdminRoute();
    }
});

Route::group('/member', function() {
    if (!Sentry::check()) {
        if (Request::isAjax()) {
            Response::headers() -> set('Content-Type', 'application/json');
            Response::setBody(json_encode(array('success' => false, 'message' => 'Session expired or unauthorized access.', 'code' => 401)));
            App::stop();
        } else {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    } else {
        $user = Sentry::getUser();
        if ($user -> hasAnyAccess(array('admin', 'cashier'))) {
            $redirect = Request::getResourceUri();
            Response::redirect(App::urlFor('login') . '?redirect=' . base64_encode($redirect));
        }
    }
}, function() use ($app) {
    /** sample namespaced controller */
    Route::get('/', 'Member\MemberController:index') -> name('member');
    foreach (Module::getModules() as $module) {
        $module -> registerPublicRoute();
    }
});

/** API ROUTES **/
Route::get('/girl_in_my_dream', 'APIController:showDreamGirl');
Route::get('/api/geneology/:id/:table', 'APIController:getGeneology');
Route::get('/api/geneology/detail/user/:user_id', 'APIController:getUserDetail');
Route::get('/api/validate/dr/:username', 'APIController:validateDR');
Route::get('/api/generate/codes/count/:count', 'APIController:generateCodes');

Route::get('/generate/lucky/lotto/:series_count/:max_digit', 'APIController:lucky_lotto_winning_number');

Route::post('/webhookHandler/:wallet_address', 'APIController:webhookHandler');

/** GET METHOD **/
Route::get('/', 'UserController:landingPage');
Route::get('/login', 'UserController:login') -> name('login');
Route::get('/logout', 'UserController:logout') -> name('logout');
Route::get('/registration', 'UserController:registration');
Route::get('/profile/:user_id', 'UserController:userProfile_view');
Route::get('/email/verification/resend/:user_id', 'UserController:resendVerificationCode');
Route::get('/registration/confirmation/:user_id/:activation_code', 'UserController:accountActivationView');

/** POST METHOD **/
Route::post('/login', 'UserController:doLogin') -> name('do_login');
Route::post('/login/guest', 'UserController:loginAsGuest') -> name('login_as_guest');
Route::post('/pre_registration', 'UserController:preRegistration') -> name('pre_registration');
Route::post('/activate_account', 'UserController:activateAccount') -> name('activate_account');
Route::post('/change/masterpassword', 'Admin\AdminController:changeMasterPassword') -> name('change_master');
Route::post('/change/adminpassword', 'Admin\AdminController:changePassword') -> name('change_admin_password');
Route::post('/toggleregistration', 'Admin\AdminController:toggleRegistration') -> name('toggle_registration');
Route::post('/togglepayout', 'Admin\AdminController:togglePayout') -> name('toggle_payout');
Route::post('/update/basic_info', 'UserController:updateBasicInfo') -> name('update_basic_info');
Route::post('/update/bank_details', 'UserController:updateBankDetails') -> name('update_bank_details');
Route::post('/update/contact_details', 'UserController:updateContactDetails') -> name('update_contact_details');
Route::post('/update/password', 'UserController:updatePassword') -> name('update_password');
Route::post('/update/pin_code', 'UserController:updatePinCode') -> name('update_pin_code');
Route::post('/update/avatar', 'UserController:updateAvatar') -> name('update_avatar');