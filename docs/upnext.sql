-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 30, 2017 at 01:53 PM
-- Server version: 5.7.12-1~exp1+deb.sury.org~trusty+1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `upnext`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_control`
--

CREATE TABLE `admin_control` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_control`
--

INSERT INTO `admin_control` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'member_registration', '0', '2015-08-17 06:06:59', '2015-08-17 21:06:59'),
(2, 'member_payout', '0', '2015-08-25 07:32:19', '2015-08-25 22:32:19');

-- --------------------------------------------------------

--
-- Table structure for table `bulletin`
--

CREATE TABLE `bulletin` (
  `id` int(8) NOT NULL,
  `message` text NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created_by` int(8) DEFAULT '80',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `codes`
--

CREATE TABLE `codes` (
  `id` int(11) NOT NULL,
  `generated_code` varchar(45) DEFAULT NULL,
  `purchase_code_id` int(11) DEFAULT NULL,
  `user_id` int(8) DEFAULT '0',
  `is_used` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `codes`
--

INSERT INTO `codes` (`id`, `generated_code`, `purchase_code_id`, `user_id`, `is_used`, `created_at`, `updated_at`) VALUES
(1, '2017ZPUeFL-0521fnSaiB-131850ZB3Q', 1, 7, 1, '2017-05-21 05:18:50', '2017-05-21 05:20:13'),
(2, '2017KucQ9E-05210qm8jr-150349EWWE', 2, 8, 1, '2017-05-21 07:03:49', '2017-05-21 07:12:12');

-- --------------------------------------------------------

--
-- Table structure for table `constants`
--

CREATE TABLE `constants` (
  `id` int(8) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '""',
  `value` varchar(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `constants`
--

INSERT INTO `constants` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'entry_amount', '12000', '2015-05-12 15:00:06', '2015-05-12 15:00:06'),
(2, 'direct_referral_bonus', '0', '2015-05-17 14:51:19', '2015-05-17 14:51:19'),
(3, 'exit_bonus', '36000', '2015-05-18 19:56:52', '2015-05-18 19:56:52'),
(4, 'misc', '0', '2015-05-18 19:57:14', '2015-05-18 19:57:14'),
(5, 'conceptualization_fee', '0', '2015-05-18 19:57:42', '2015-05-18 19:57:42'),
(6, 'pres_share', '0', '2015-05-18 19:58:02', '2015-05-18 19:58:02'),
(7, 'vpres_share', '0', '2015-05-18 19:58:27', '2015-05-18 19:58:27'),
(8, 'company_share', '0', '2015-05-18 19:58:54', '2015-05-18 19:58:54');

-- --------------------------------------------------------

--
-- Table structure for table `direct_referrals`
--

CREATE TABLE `direct_referrals` (
  `id` int(11) NOT NULL,
  `recruiter_id` int(11) DEFAULT NULL,
  `recruitee_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `direct_referrals`
--

INSERT INTO `direct_referrals` (`id`, `recruiter_id`, `recruitee_id`, `created_at`, `updated_at`) VALUES
(1, 6, 7, '2017-05-21 05:20:13', '2017-05-21 05:20:13'),
(2, 6, 8, '2017-05-21 07:12:12', '2017-05-21 07:12:12');

-- --------------------------------------------------------

--
-- Table structure for table `exit_history`
--

CREATE TABLE `exit_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `value` decimal(10,2) NOT NULL DEFAULT '10000.00',
  `table` int(8) DEFAULT '1',
  `level` int(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` int(11) NOT NULL,
  `module_name` text,
  `content` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `module_name`, `content`, `created_at`, `updated_at`) VALUES
(1, 'wallet_address', '{"value":"2N7t14vJE6KxGooZ2EeERpokzWzLRi8hyBu"}', '2016-09-09 03:54:22', NULL),
(2, 'wallet_username', '{"value":"company_wallet"}', '2016-09-09 03:54:40', NULL),
(3, 'wallet_password', '{"value":"UpNext-Trading"}', '2016-09-09 03:54:53', NULL),
(4, 'codes_amount', '{"value":"1500"}', '2016-09-09 03:54:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `permissions` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Administrators', '{"admin":1}', '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(2, 'Members', '{"member":1}', '2016-11-30 16:56:59', '2016-11-30 16:56:59');

-- --------------------------------------------------------

--
-- Table structure for table `hierarchy_siblings`
--

CREATE TABLE `hierarchy_siblings` (
  `id` int(11) NOT NULL,
  `recruitee_id` int(11) NOT NULL,
  `position` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hierarchy_siblings`
--

INSERT INTO `hierarchy_siblings` (`id`, `recruitee_id`, `position`, `created_at`, `updated_at`) VALUES
(1, 7, 0, '2017-05-21 05:20:13', '2017-05-21 05:20:13'),
(2, 8, 1, '2017-05-21 07:12:12', '2017-05-21 07:12:12');

-- --------------------------------------------------------

--
-- Table structure for table `master_password`
--

CREATE TABLE `master_password` (
  `id` int(11) NOT NULL,
  `password` varchar(255) DEFAULT '',
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_password`
--

INSERT INTO `master_password` (`id`, `password`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'eb0a191797624dd3a48fa681d3061212', 1, '2015-05-24 11:19:47', '2015-06-17 01:36:33');

-- --------------------------------------------------------

--
-- Table structure for table `member_payout_requests`
--

CREATE TABLE `member_payout_requests` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `payout_method` text,
  `table` int(1) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_code`
--

CREATE TABLE `purchase_code` (
  `id` int(11) NOT NULL,
  `reference_code` varchar(250) DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `total_amount` decimal(10,2) DEFAULT '0.00',
  `btc_amount` decimal(10,8) DEFAULT '0.00000000',
  `payment_method` tinyint(1) DEFAULT '1' COMMENT '1 = cash, 2 = btc',
  `quantity` int(11) DEFAULT NULL,
  `code_amount` decimal(10,2) DEFAULT '0.00',
  `approved_by` int(11) DEFAULT NULL,
  `remarks` text,
  `status` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_code`
--

INSERT INTO `purchase_code` (`id`, `reference_code`, `user_id`, `total_amount`, `btc_amount`, `payment_method`, `quantity`, `code_amount`, `approved_by`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 'E4L8Hw2wRZ', 7, '1500.00', '0.00000000', 1, 1, '1500.00', 1, 'payamanan', 1, '2017-05-21 05:16:08', '2017-05-21 05:18:50'),
(2, 'yN2YerztC0', 8, '1500.00', '0.00000000', 1, 1, '1500.00', 1, 'ssdfasdfadsfasdfasdf', 1, '2017-05-21 07:02:56', '2017-05-21 07:03:49');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_code_proof`
--

CREATE TABLE `purchase_code_proof` (
  `id` int(11) NOT NULL,
  `purchase_code_id` int(11) DEFAULT NULL,
  `file_name` varchar(500) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_code_proof`
--

INSERT INTO `purchase_code_proof` (`id`, `purchase_code_id`, `file_name`, `created_at`, `updated_at`) VALUES
(1, 2, 'assets/images/purchase_proof/8/d5adb6ad5c3245a3b68afd113b8c326f.jpg', '2017-05-21 07:02:56', '2017-05-21 07:02:56');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `attempts` varchar(45) DEFAULT NULL,
  `suspended` tinyint(1) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT NULL,
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `ref_code` varchar(255) DEFAULT 'XXXX-XXXX-XXXX',
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `canonical_hash` varchar(500) DEFAULT NULL,
  `pin_code` int(4) DEFAULT '1234',
  `full_name` varchar(255) DEFAULT '',
  `avatar` varchar(500) DEFAULT 'default-avatar.png',
  `birthday` date DEFAULT NULL,
  `address` text,
  `contact_no` varchar(255) DEFAULT NULL,
  `permissions` text,
  `user_type` tinyint(1) DEFAULT '0' COMMENT '1=Admin, 2=Member',
  `activated` tinyint(1) DEFAULT '0',
  `activation_code` varchar(255) DEFAULT '',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) DEFAULT NULL,
  `reset_password_code` varchar(255) DEFAULT NULL,
  `is_registered` tinyint(1) DEFAULT '0',
  `company_associated_wallet` varchar(250) DEFAULT NULL,
  `is_primary` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ref_code`, `username`, `email`, `password`, `canonical_hash`, `pin_code`, `full_name`, `avatar`, `birthday`, `address`, `contact_no`, `permissions`, `user_type`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `is_registered`, `company_associated_wallet`, `is_primary`, `created_at`, `updated_at`) VALUES
(1, 'u61J14805250195Agq', 'admin1', 'admin1@upnext.shop', '$2y$10$JwPNRtXKzz7xwomyeNeH1uiIjTIjlpyPNa57xMUGSezshmOoCKPqq', 'YWRtaW4x', 1234, 'Administrator 1', 'default-avatar.png', NULL, NULL, NULL, '{"admin":1}', 1, 1, '', NULL, '2017-05-21 07:03:28', '$2y$10$lSQp56b7mubt.Y1e0vPoo.vcSTZZT7OHJDoDDVgVRV62qToBebMOO', NULL, 1, NULL, 1, '2016-11-30 16:56:59', '2017-05-21 07:03:28'),
(2, 'C14U14805250194l4c', 'admin2', 'admin2@upnext.shop', '$2y$10$fXbQAu.h4q0hXbp/8curlO994aYAWfWee06OCx8AbqG.eGOo97Zv6', 'YWRtaW4y', 1234, 'Administrator 2', 'default-avatar.png', NULL, NULL, NULL, '{"admin":1}', 1, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(3, 'eLFx1480525019skte', 'admin3', 'admin3@upnext.shop', '$2y$10$sqMt9lOvW/3YWcBOzzyaOuj/fXnZDM4LV8FQIJQ5BtJFlrid/B/hq', 'YWRtaW4z', 1234, 'Administrator 3', 'default-avatar.png', NULL, NULL, NULL, '{"admin":1}', 1, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(4, 'b3Fq1480525019tIyn', 'admin4', 'admin4@upnext.shop', '$2y$10$MMtqDhjtieoTRo2lZLJ68Ok.IvqhmvVte3szKKRos.KvJ1pVcta56', 'YWRtaW40', 1234, 'Administrator 4', 'default-avatar.png', NULL, NULL, NULL, '{"admin":1}', 1, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(5, 'YW6b1480525019KyKX', 'admin5', 'admin5@upnext.shop', '$2y$10$UAnctLyJtnViiSECjuDMzuGTEHyflFEqJKsWGcpWh6MQ8A2k7wO9i', 'YWRtaW41', 1234, 'Administrator 5', 'default-avatar.png', NULL, NULL, NULL, '{"admin":1}', 1, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(6, 'JEA11480525020RYeB', 'upnext', 'support@upnext.shop', '$2y$10$DDNNMnMfX2sE4lPRICy2Vem0oHrbRHwworJ3GCS8aKSautqj1pQBi', 'cGFzc3dvcmQxMjM0', 1234, 'Company Head', 'default-avatar.png', NULL, NULL, NULL, '{"member":1}', 2, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, '2016-11-30 16:57:00', '2016-11-30 16:57:00'),
(7, '3FuH1480525186WgSj', 'ullen', 'ullen.d.faustino@gmail.com', '$2y$10$F15Ax4h/Sqj3YpOd.cTF4OZxJA4nuuq8Xo3I5mkxVwpG2lAy9FNM6', 'Z3F5RENMQVJYaVdQ', 1234, 'Ullen D. Faustino', 'default-avatar.png', '2016-12-19', 'San Jose Balange City Bataan', '342-42-3423423', '{"member":1}', 2, 1, NULL, '2016-12-19 03:42:21', '2017-05-21 05:19:16', '$2y$10$ZYm0/JhHWHYK7P9WBHaziOwzOD6PheFXI2Yc1hN9/6QvIxLGF5IeK', NULL, 1, '2N2RCvcMKZXaJ6wvNUeVY337zu6K1koJZCR', 1, '2016-11-30 16:59:46', '2017-05-21 05:20:13'),
(8, 'Zf8n1495347829ySD5', 'julz', 'ullen.d.faustino@gmail.com', '$2y$10$bEmKKW19AFm7hziYO30b6.0pGnJLfAs/IQPp8CxhgCCjrrBYyanBG', 'U3l6dE12Q2ZhYUdG', 1234, 'julz millton', 'd5adb6ad5c3245a3b68afd113b8c326f.jpg', '2017-05-15', 'San Jose', '242-43-2423423', '{"member":1}', 2, 1, NULL, '2017-05-21 06:57:50', '2017-05-21 08:31:26', '$2y$10$/qPxGBVZcNAyh0ilphdMY.WXkWTbfUYVqK.CiT8gT02B43KneekkW', NULL, 1, NULL, 1, '2017-05-21 06:23:49', '2017-05-21 08:31:37');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 2),
(7, 2),
(8, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users_has_hierarchy_siblings`
--

CREATE TABLE `users_has_hierarchy_siblings` (
  `user_id` int(11) NOT NULL,
  `hierarchy_sibling_id` int(11) NOT NULL,
  `table` int(8) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_has_hierarchy_siblings`
--

INSERT INTO `users_has_hierarchy_siblings` (`user_id`, `hierarchy_sibling_id`, `table`) VALUES
(6, 1, 1),
(6, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_balance`
--

CREATE TABLE `user_balance` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pending_balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `table` int(1) DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_control`
--
ALTER TABLE `admin_control`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bulletin`
--
ALTER TABLE `bulletin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `codes`
--
ALTER TABLE `codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `constants`
--
ALTER TABLE `constants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `direct_referrals`
--
ALTER TABLE `direct_referrals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exit_history`
--
ALTER TABLE `exit_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hierarchy_siblings`
--
ALTER TABLE `hierarchy_siblings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_has_users_users4_idx` (`recruitee_id`);

--
-- Indexes for table `master_password`
--
ALTER TABLE `master_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_payout_requests`
--
ALTER TABLE `member_payout_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_code`
--
ALTER TABLE `purchase_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_code_proof`
--
ALTER TABLE `purchase_code_proof`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD KEY `fk_throttle_users_idx` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `fk_users_group_groups1_idx` (`group_id`);

--
-- Indexes for table `users_has_hierarchy_siblings`
--
ALTER TABLE `users_has_hierarchy_siblings`
  ADD PRIMARY KEY (`user_id`,`hierarchy_sibling_id`),
  ADD KEY `fk_users_has_hierarchy_children_hierarchy_children1_idx` (`hierarchy_sibling_id`),
  ADD KEY `fk_users_has_hierarchy_children_users1_idx` (`user_id`);

--
-- Indexes for table `user_balance`
--
ALTER TABLE `user_balance`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_control`
--
ALTER TABLE `admin_control`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `bulletin`
--
ALTER TABLE `bulletin`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `codes`
--
ALTER TABLE `codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `constants`
--
ALTER TABLE `constants`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `direct_referrals`
--
ALTER TABLE `direct_referrals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `exit_history`
--
ALTER TABLE `exit_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hierarchy_siblings`
--
ALTER TABLE `hierarchy_siblings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `master_password`
--
ALTER TABLE `master_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `member_payout_requests`
--
ALTER TABLE `member_payout_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase_code`
--
ALTER TABLE `purchase_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `purchase_code_proof`
--
ALTER TABLE `purchase_code_proof`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_balance`
--
ALTER TABLE `user_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `hierarchy_siblings`
--
ALTER TABLE `hierarchy_siblings`
  ADD CONSTRAINT `fk_users_has_users_users4` FOREIGN KEY (`recruitee_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `throttle`
--
ALTER TABLE `throttle`
  ADD CONSTRAINT `fk_throttle_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_group_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_group_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_has_hierarchy_siblings`
--
ALTER TABLE `users_has_hierarchy_siblings`
  ADD CONSTRAINT `fk_users_has_hierarchy_children_hierarchy_children1` FOREIGN KEY (`hierarchy_sibling_id`) REFERENCES `hierarchy_siblings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_has_hierarchy_children_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
