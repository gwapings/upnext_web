-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 31, 2017 at 07:43 PM
-- Server version: 5.7.12-1~exp1+deb.sury.org~trusty+1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `upnext`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_control`
--

CREATE TABLE `admin_control` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_control`
--

INSERT INTO `admin_control` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'member_registration', '0', '2015-08-17 06:06:59', '2015-08-17 21:06:59'),
(2, 'member_payout', '0', '2015-08-25 07:32:19', '2015-08-25 22:32:19');

-- --------------------------------------------------------

--
-- Table structure for table `bulletin`
--

CREATE TABLE `bulletin` (
  `id` int(8) NOT NULL,
  `message` text NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created_by` int(8) DEFAULT '80',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `codes`
--

CREATE TABLE `codes` (
  `id` int(11) NOT NULL,
  `generated_code` varchar(45) DEFAULT NULL,
  `purchase_code_id` int(11) DEFAULT NULL,
  `user_id` int(8) DEFAULT '0',
  `is_used` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `codes`
--

INSERT INTO `codes` (`id`, `generated_code`, `purchase_code_id`, `user_id`, `is_used`, `created_at`, `updated_at`) VALUES
(1, '2017QwsZXH-0531JgnQLR-1500505YZ7', 1, 7, 1, '2017-05-31 07:00:50', '2017-05-31 07:36:38'),
(2, '2017ALu5iM-0531SXFf0c-155040BZ29', 2, 8, 1, '2017-05-31 07:50:40', '2017-05-31 09:44:35'),
(3, '2017wDUwgn-0531jnn3ww-155040Q2HM', 2, 9, 1, '2017-05-31 07:50:40', '2017-05-31 09:49:53'),
(4, '2017cETbfm-0531bYS6xG-1550408UIV', 2, 10, 1, '2017-05-31 07:50:40', '2017-05-31 09:57:46'),
(5, '201748k8dX-0531KvsdE7-155040DWZV', 2, 0, 0, '2017-05-31 07:50:40', '2017-05-31 07:50:40'),
(6, '2017aXuAn2-05315BkFIi-155040Q9A6', 2, 0, 0, '2017-05-31 07:50:40', '2017-05-31 07:50:40'),
(7, '2017vJ13B1-0531KfYvnj-155040XT9Z', 2, 0, 0, '2017-05-31 07:50:40', '2017-05-31 07:50:40'),
(8, '20171uRa9v-0531vfdjev-15504091TG', 2, 0, 0, '2017-05-31 07:50:40', '2017-05-31 07:50:40'),
(9, '2017gib5Ai-0531AJhEcZ-155040CTGW', 2, 0, 0, '2017-05-31 07:50:40', '2017-05-31 07:50:40'),
(10, '2017RsqpYj-0531e4zumN-155040CXTI', 2, 0, 0, '2017-05-31 07:50:40', '2017-05-31 07:50:40'),
(11, '20178jGbfh-0531J7NKXM-155040RHY1', 2, 0, 0, '2017-05-31 07:50:40', '2017-05-31 07:50:40'),
(12, '2017GfZgVt-0531e9wlEH-155040I17G', 2, 0, 0, '2017-05-31 07:50:40', '2017-05-31 07:50:40'),
(13, '2017WmCs2R-05312Lelzt-155040ENUW', 2, 0, 0, '2017-05-31 07:50:40', '2017-05-31 07:50:40'),
(14, '20174SgZ4s-05318DxXHJ-155040V8VQ', 2, 0, 0, '2017-05-31 07:50:40', '2017-05-31 07:50:40'),
(15, '20177EQ3Ar-0531LvxLWy-155040QV7V', 2, 0, 0, '2017-05-31 07:50:40', '2017-05-31 07:50:40'),
(16, '2017H9J9ZT-0531IiwWtv-155041CXKP', 2, 0, 0, '2017-05-31 07:50:41', '2017-05-31 07:50:41'),
(17, '20172uYzuV-0531INqJ4j-155041CELX', 2, 0, 0, '2017-05-31 07:50:41', '2017-05-31 07:50:41'),
(18, '2017vQ4X5I-0531ndTF0L-1550416HS3', 2, 0, 0, '2017-05-31 07:50:41', '2017-05-31 07:50:41'),
(19, '2017uf83WM-0531K5cnv5-155041RMUA', 2, 0, 0, '2017-05-31 07:50:41', '2017-05-31 07:50:41'),
(20, '2017whIwV9-0531HMGpxQ-155041DKRU', 2, 0, 0, '2017-05-31 07:50:41', '2017-05-31 07:50:41'),
(21, '20172ekssC-0531BEbSvN-155041DNCS', 2, 0, 0, '2017-05-31 07:50:41', '2017-05-31 07:50:41'),
(22, '2017k4Jrdy-0531lT0DQh-155041B0Z6', 2, 0, 0, '2017-05-31 07:50:41', '2017-05-31 07:50:41'),
(23, '2017RNdTXe-0531VYJUTz-155041XJS3', 2, 0, 0, '2017-05-31 07:50:41', '2017-05-31 07:50:41'),
(24, '2017g6c1Z6-0531vEMVaB-15504171U0', 2, 19, 1, '2017-05-31 07:50:41', '2017-05-31 11:19:09'),
(25, '2017i4HP0Z-0531w2iARy-1550412QYK', 2, 17, 1, '2017-05-31 07:50:41', '2017-05-31 11:18:00'),
(26, '2017XmkibU-0531kDrQn8-155041KAJG', 2, 15, 1, '2017-05-31 07:50:41', '2017-05-31 11:15:56'),
(27, '2017wf1MRC-0531GuAt6f-155041N98W', 2, 13, 1, '2017-05-31 07:50:41', '2017-05-31 11:14:05'),
(28, '2017N7ujfE-0531EDpG5W-155041P9T4', 2, 11, 1, '2017-05-31 07:50:41', '2017-05-31 11:10:09'),
(29, '2017zDZdv8-0531U3fUHU-155041ABUF', 2, 12, 1, '2017-05-31 07:50:41', '2017-05-31 11:13:30'),
(30, '20176qUugs-05310vC7uR-15504171L8', 2, 14, 1, '2017-05-31 07:50:41', '2017-05-31 11:14:47'),
(31, '20174mTRbL-05317mJsLd-155041Q5V4', 2, 16, 1, '2017-05-31 07:50:41', '2017-05-31 11:16:46'),
(32, '2017ISRZYj-0531hgSMlz-1550418NP8', 2, 18, 1, '2017-05-31 07:50:41', '2017-05-31 11:18:40'),
(33, '2017iFMR4M-0531pTH0gG-1550419L5R', 2, 20, 1, '2017-05-31 07:50:41', '2017-05-31 11:20:04'),
(34, '2017pyH2vL-0531B6kS8r-1550416IJ2', 2, 0, 0, '2017-05-31 07:50:41', '2017-05-31 07:50:41'),
(35, '2017l1693Q-0531A20yqx-155041CZBQ', 2, 0, 0, '2017-05-31 07:50:41', '2017-05-31 07:50:41'),
(36, '2017BF5tkM-0531jauJJs-155041P1D8', 2, 0, 0, '2017-05-31 07:50:41', '2017-05-31 07:50:41'),
(37, '20176IiyKI-0531vB6jYV-155042EPVZ', 2, 0, 0, '2017-05-31 07:50:42', '2017-05-31 07:50:42'),
(38, '2017i3166D-053193QuUN-155042RFYP', 2, 0, 0, '2017-05-31 07:50:42', '2017-05-31 07:50:42'),
(39, '2017JbrawD-0531wiDa0R-1550422T4Z', 2, 0, 0, '2017-05-31 07:50:42', '2017-05-31 07:50:42'),
(40, '2017tUS0qd-0531Zd0LaV-155042DZ31', 2, 0, 0, '2017-05-31 07:50:42', '2017-05-31 07:50:42'),
(41, '20170TURsv-0531uitUMn-155042EIRV', 2, 0, 0, '2017-05-31 07:50:42', '2017-05-31 07:50:42'),
(42, '2017hKXm50-0531fedxNq-1550425MB3', 2, 0, 0, '2017-05-31 07:50:42', '2017-05-31 07:50:42'),
(43, '20173SvPDS-0531mmjrQr-155042BE20', 2, 0, 0, '2017-05-31 07:50:42', '2017-05-31 07:50:42'),
(44, '2017kHpFF7-0531GM9H1T-155042IMQ6', 2, 0, 0, '2017-05-31 07:50:42', '2017-05-31 07:50:42'),
(45, '2017vidXZ2-0531K7g4GG-155042P4MJ', 2, 0, 0, '2017-05-31 07:50:42', '2017-05-31 07:50:42'),
(46, '2017yA40AV-0531fWdAzZ-1550427S7E', 2, 0, 0, '2017-05-31 07:50:42', '2017-05-31 07:50:42'),
(47, '2017nl11Cn-0531DvNdPn-155042LXTV', 2, 0, 0, '2017-05-31 07:50:42', '2017-05-31 07:50:42'),
(48, '2017Dv39b1-0531dkQnml-155043SY3L', 2, 0, 0, '2017-05-31 07:50:43', '2017-05-31 07:50:43'),
(49, '2017vCiNSa-0531qnF5KL-155043E6BJ', 2, 0, 0, '2017-05-31 07:50:43', '2017-05-31 07:50:43'),
(50, '2017Zx4mXa-0531lehfc0-155043B84L', 2, 0, 0, '2017-05-31 07:50:43', '2017-05-31 07:50:43'),
(51, '2017Y06frA-0531WgRsn4-155043MV81', 2, 0, 0, '2017-05-31 07:50:43', '2017-05-31 07:50:43');

-- --------------------------------------------------------

--
-- Table structure for table `constants`
--

CREATE TABLE `constants` (
  `id` int(8) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '""',
  `value` varchar(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `constants`
--

INSERT INTO `constants` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'entry_amount', '12000', '2015-05-12 15:00:06', '2015-05-12 15:00:06'),
(2, 'direct_referral_bonus', '0', '2015-05-17 14:51:19', '2015-05-17 14:51:19'),
(3, 'exit_bonus', '36000', '2015-05-18 19:56:52', '2015-05-18 19:56:52'),
(4, 'misc', '0', '2015-05-18 19:57:14', '2015-05-18 19:57:14'),
(5, 'conceptualization_fee', '0', '2015-05-18 19:57:42', '2015-05-18 19:57:42'),
(6, 'pres_share', '0', '2015-05-18 19:58:02', '2015-05-18 19:58:02'),
(7, 'vpres_share', '0', '2015-05-18 19:58:27', '2015-05-18 19:58:27'),
(8, 'company_share', '0', '2015-05-18 19:58:54', '2015-05-18 19:58:54');

-- --------------------------------------------------------

--
-- Table structure for table `direct_referrals`
--

CREATE TABLE `direct_referrals` (
  `id` int(11) NOT NULL,
  `recruiter_id` int(11) DEFAULT NULL,
  `recruitee_id` int(11) DEFAULT NULL,
  `dr_earnings` decimal(10,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `direct_referrals`
--

INSERT INTO `direct_referrals` (`id`, `recruiter_id`, `recruitee_id`, `dr_earnings`, `created_at`, `updated_at`) VALUES
(1, 6, 7, '100.00', '2017-05-31 07:36:38', '2017-05-31 07:36:38'),
(2, 6, 8, '100.00', '2017-05-31 09:44:35', '2017-05-31 09:44:35'),
(3, 7, 9, '100.00', '2017-05-31 09:49:53', '2017-05-31 09:49:53'),
(4, 6, 10, '100.00', '2017-05-31 09:57:46', '2017-05-31 09:57:46'),
(5, 6, 11, '100.00', '2017-05-31 11:10:09', '2017-05-31 11:10:09'),
(6, 6, 12, '100.00', '2017-05-31 11:13:30', '2017-05-31 11:13:30'),
(7, 6, 13, '100.00', '2017-05-31 11:14:05', '2017-05-31 11:14:05'),
(8, 6, 14, '100.00', '2017-05-31 11:14:47', '2017-05-31 11:14:47'),
(9, 6, 15, '100.00', '2017-05-31 11:15:56', '2017-05-31 11:15:56'),
(10, 6, 16, '100.00', '2017-05-31 11:16:46', '2017-05-31 11:16:46'),
(11, 6, 17, '100.00', '2017-05-31 11:17:59', '2017-05-31 11:17:59'),
(12, 6, 18, '100.00', '2017-05-31 11:18:40', '2017-05-31 11:18:40'),
(13, 6, 19, '100.00', '2017-05-31 11:19:09', '2017-05-31 11:19:09'),
(14, 6, 20, '100.00', '2017-05-31 11:20:04', '2017-05-31 11:20:04');

-- --------------------------------------------------------

--
-- Table structure for table `exit_history`
--

CREATE TABLE `exit_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `value` decimal(10,2) NOT NULL DEFAULT '10000.00',
  `table` int(8) DEFAULT '1',
  `level` int(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` int(11) NOT NULL,
  `module_name` text,
  `content` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `module_name`, `content`, `created_at`, `updated_at`) VALUES
(1, 'wallet_address', '{"value":"2N7t14vJE6KxGooZ2EeERpokzWzLRi8hyBu"}', '2016-09-09 03:54:22', NULL),
(2, 'wallet_username', '{"value":"company_wallet"}', '2016-09-09 03:54:40', NULL),
(3, 'wallet_password', '{"value":"UpNext-Trading"}', '2016-09-09 03:54:53', NULL),
(4, 'codes_amount', '{"value":"1500"}', '2016-09-09 03:54:53', NULL),
(5, 'dr_bonus', '{"value":"100"}', '2016-09-09 03:54:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `permissions` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Administrators', '{"admin":1}', '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(2, 'Members', '{"member":1}', '2016-11-30 16:56:59', '2016-11-30 16:56:59');

-- --------------------------------------------------------

--
-- Table structure for table `hierarchy_siblings`
--

CREATE TABLE `hierarchy_siblings` (
  `id` int(11) NOT NULL,
  `recruitee_id` int(11) NOT NULL,
  `position` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hierarchy_siblings`
--

INSERT INTO `hierarchy_siblings` (`id`, `recruitee_id`, `position`, `created_at`, `updated_at`) VALUES
(1, 7, 1, '2017-05-31 07:36:38', '2017-05-31 07:36:38'),
(2, 8, 0, '2017-05-31 09:44:35', '2017-05-31 09:44:35'),
(3, 9, 1, '2017-05-31 09:49:53', '2017-05-31 09:49:53'),
(4, 10, 0, '2017-05-31 09:57:46', '2017-05-31 09:57:46'),
(5, 11, 1, '2017-05-31 11:10:09', '2017-05-31 11:10:09'),
(6, 12, 0, '2017-05-31 11:13:30', '2017-05-31 11:13:30'),
(7, 13, 1, '2017-05-31 11:14:05', '2017-05-31 11:14:05'),
(8, 14, 0, '2017-05-31 11:14:46', '2017-05-31 11:14:46'),
(9, 15, 1, '2017-05-31 11:15:56', '2017-05-31 11:15:56'),
(10, 16, 0, '2017-05-31 11:16:46', '2017-05-31 11:16:46'),
(11, 17, 1, '2017-05-31 11:17:59', '2017-05-31 11:17:59'),
(12, 18, 0, '2017-05-31 11:18:40', '2017-05-31 11:18:40'),
(13, 19, 1, '2017-05-31 11:19:09', '2017-05-31 11:19:09'),
(14, 20, 0, '2017-05-31 11:20:04', '2017-05-31 11:20:04');

-- --------------------------------------------------------

--
-- Table structure for table `master_password`
--

CREATE TABLE `master_password` (
  `id` int(11) NOT NULL,
  `password` varchar(255) DEFAULT '',
  `is_active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_password`
--

INSERT INTO `master_password` (`id`, `password`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'eb0a191797624dd3a48fa681d3061212', 1, '2015-05-24 11:19:47', '2015-06-17 01:36:33');

-- --------------------------------------------------------

--
-- Table structure for table `member_payout_requests`
--

CREATE TABLE `member_payout_requests` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `payout_method` text,
  `table` int(1) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_code`
--

CREATE TABLE `purchase_code` (
  `id` int(11) NOT NULL,
  `reference_code` varchar(250) DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `total_amount` decimal(10,2) DEFAULT '0.00',
  `btc_amount` decimal(10,8) DEFAULT '0.00000000',
  `payment_method` tinyint(1) DEFAULT '1' COMMENT '1 = cash, 2 = btc',
  `quantity` int(11) DEFAULT NULL,
  `code_amount` decimal(10,2) DEFAULT '0.00',
  `approved_by` int(11) DEFAULT NULL,
  `remarks` text,
  `status` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_code`
--

INSERT INTO `purchase_code` (`id`, `reference_code`, `user_id`, `total_amount`, `btc_amount`, `payment_method`, `quantity`, `code_amount`, `approved_by`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 'IKgFFCDcRD', 7, '1500.00', '0.00000000', 1, 1, '1500.00', 1, '', 1, '2017-05-31 07:00:27', '2017-05-31 07:00:51'),
(2, 'WFDxL6qw8U', 7, '75000.00', '0.00000000', 1, 50, '1500.00', 1, '', 1, '2017-05-31 07:50:06', '2017-05-31 07:50:43');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_code_proof`
--

CREATE TABLE `purchase_code_proof` (
  `id` int(11) NOT NULL,
  `purchase_code_id` int(11) DEFAULT NULL,
  `file_name` varchar(500) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_code_proof`
--

INSERT INTO `purchase_code_proof` (`id`, `purchase_code_id`, `file_name`, `created_at`, `updated_at`) VALUES
(1, 1, 'assets/images/purchase_proof/7/65cfd77227b433134bdd5e13c9b7d4df.jpg', '2017-05-31 07:00:27', '2017-05-31 07:00:27'),
(2, 2, 'assets/images/purchase_proof/7/213065b19b50c181f48c9d64f1221b61.jpg', '2017-05-31 07:50:06', '2017-05-31 07:50:06');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `attempts` varchar(45) DEFAULT NULL,
  `suspended` tinyint(1) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT NULL,
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `ref_code` varchar(255) DEFAULT 'XXXX-XXXX-XXXX',
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `canonical_hash` varchar(500) DEFAULT NULL,
  `pin_code` int(4) DEFAULT '1234',
  `full_name` varchar(255) DEFAULT '',
  `avatar` varchar(500) DEFAULT 'default-avatar.png',
  `birthday` date DEFAULT NULL,
  `address` text,
  `contact_no` varchar(255) DEFAULT NULL,
  `permissions` text,
  `user_type` tinyint(1) DEFAULT '0' COMMENT '1=Admin, 2=Member',
  `activated` tinyint(1) DEFAULT '0',
  `activation_code` varchar(255) DEFAULT '',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) DEFAULT NULL,
  `reset_password_code` varchar(255) DEFAULT NULL,
  `is_registered` tinyint(1) DEFAULT '0',
  `company_associated_wallet` varchar(250) DEFAULT NULL,
  `is_primary` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ref_code`, `username`, `email`, `password`, `canonical_hash`, `pin_code`, `full_name`, `avatar`, `birthday`, `address`, `contact_no`, `permissions`, `user_type`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `is_registered`, `company_associated_wallet`, `is_primary`, `created_at`, `updated_at`) VALUES
(1, 'u61J14805250195Agq', 'admin1', 'admin1@upnext.shop', '$2y$10$JwPNRtXKzz7xwomyeNeH1uiIjTIjlpyPNa57xMUGSezshmOoCKPqq', 'YWRtaW4x', 1234, 'Administrator 1', 'default-avatar.png', NULL, NULL, NULL, '{"admin":1}', 1, 1, '', NULL, '2017-05-31 07:55:41', '$2y$10$z0FGySHj6pkNi7jZSVwuV.MV.qUir1PSLwayTtWcmSLcOj9O9u2Ym', NULL, 1, NULL, 1, '2016-11-30 16:56:59', '2017-05-31 07:55:41'),
(2, 'C14U14805250194l4c', 'admin2', 'admin2@upnext.shop', '$2y$10$fXbQAu.h4q0hXbp/8curlO994aYAWfWee06OCx8AbqG.eGOo97Zv6', 'YWRtaW4y', 1234, 'Administrator 2', 'default-avatar.png', NULL, NULL, NULL, '{"admin":1}', 1, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(3, 'eLFx1480525019skte', 'admin3', 'admin3@upnext.shop', '$2y$10$sqMt9lOvW/3YWcBOzzyaOuj/fXnZDM4LV8FQIJQ5BtJFlrid/B/hq', 'YWRtaW4z', 1234, 'Administrator 3', 'default-avatar.png', NULL, NULL, NULL, '{"admin":1}', 1, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(4, 'b3Fq1480525019tIyn', 'admin4', 'admin4@upnext.shop', '$2y$10$MMtqDhjtieoTRo2lZLJ68Ok.IvqhmvVte3szKKRos.KvJ1pVcta56', 'YWRtaW40', 1234, 'Administrator 4', 'default-avatar.png', NULL, NULL, NULL, '{"admin":1}', 1, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(5, 'YW6b1480525019KyKX', 'admin5', 'admin5@upnext.shop', '$2y$10$UAnctLyJtnViiSECjuDMzuGTEHyflFEqJKsWGcpWh6MQ8A2k7wO9i', 'YWRtaW41', 1234, 'Administrator 5', 'default-avatar.png', NULL, NULL, NULL, '{"admin":1}', 1, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, '2016-11-30 16:56:59', '2016-11-30 16:56:59'),
(6, 'JEA11480525020RYeB', 'upnext', 'support@upnext.shop', '$2y$10$DDNNMnMfX2sE4lPRICy2Vem0oHrbRHwworJ3GCS8aKSautqj1pQBi', 'cGFzc3dvcmQxMjM0', 1234, 'Company Head', 'default-avatar.png', NULL, NULL, NULL, '{"member":1}', 2, 1, '', NULL, NULL, NULL, NULL, 1, NULL, 1, '2016-11-30 16:57:00', '2016-11-30 16:57:00'),
(7, 'JW8w1496212766wWuJ', 'ullen', 'ullen.d.faustino@gmail.com', '$2y$10$K/S4pXSsKFFzac0seNr9Yet8db9E3Hi8y.GdjOYmVFtYgnFVfP4Pu', 'ZVl4Qm5NVDFHanJh', 1234, 'Ullen Faustino', 'default-avatar.png', '2017-05-18', 'San Jose', '123-45-6789876', '{"member":1}', 2, 1, NULL, '2017-05-31 06:44:02', '2017-05-31 11:09:02', '$2y$10$zk3QX7qCd7Muj/R3AM2wYuvoHxGuzsfhnOaCG4/MomiD0Tvkwt3LW', NULL, 1, NULL, 1, '2017-05-31 06:39:26', '2017-05-31 11:09:02'),
(8, 'NqKZ1496223874rdMa', 'julz', 'ullen.d.faustino@gmail.com', '$2y$10$s6IZRucq5d01qMX6v37PRu.YTl/UKj4UCNn9/gvBBI8PlSYroCkza', 'UTZmcHcyMTIzSTFQ', 1234, 'julz millton', 'default-avatar.png', '2017-05-25', 'sdfgsdfgfdsgdfgsdfgdsfg', '132-45-4676654', '{"member":1}', 2, 1, NULL, '2017-05-31 09:46:19', '2017-05-31 09:46:19', '$2y$10$2njoc9VqXFEfJqiAvmKngOsOH.MOAS2wO16nXoanUZMfsSxc1C0m6', NULL, 1, NULL, 1, '2017-05-31 09:44:34', '2017-05-31 09:46:19'),
(9, 'enEk1496224193Fs6P', 'juleane', 'ullen.d.faustino@gmail.com', '$2y$10$cRF7ubpBUXQ9xBwA4z1SZueUtiCKyBknczMtN/3WwVdJkZrwqm11W', 'U0JEcmdFRkRudGZQ', 1234, 'Juleane Maechelle Faustino', 'default-avatar.png', NULL, NULL, NULL, '{"member":1}', 2, 0, 'p1gBhkS59BNb5vAFNLcRC0gdGSDK3d7YlGSt4YIUNe', NULL, NULL, NULL, NULL, 1, NULL, 1, '2017-05-31 09:49:53', '2017-05-31 09:49:53'),
(10, '22SI14962246661nEK', 'a', 'ullen.d.faustino@gmail.com', '$2y$10$CKONqOtTTc4cBbExujTFZ.KFuhdIY74d.xpy1VKgT51mGrkdFkGoy', 'cVhsRG1VM0VDRHVL', 1234, 'a', 'default-avatar.png', NULL, NULL, NULL, '{"member":1}', 2, 0, 'DI5ltd8E6IJAYP7ycaILnWn29iao2ZRrGGhZizsdqz', NULL, NULL, NULL, NULL, 1, NULL, 1, '2017-05-31 09:57:46', '2017-05-31 09:57:47'),
(11, 'nzIG1496229008ABYn', 'b', 'ullen.d.faustino@gmail.com', '$2y$10$4SCo4kcc2VEMZpZKKAhM/uuwVcS2sdBQ7lsD/Sj5PFEvYli38yDMW', 'OEpkdkhEaDJNaHhB', 1234, 'b', 'default-avatar.png', NULL, NULL, NULL, '{"member":1}', 2, 0, 'JjS55XJNKnItOSlVTfDMFZT7cwSz1gNYGX5D6OnKyT', NULL, NULL, NULL, NULL, 1, NULL, 1, '2017-05-31 11:10:09', '2017-05-31 11:10:09'),
(12, 'HWxa1496229209Tibt', 'c', 'ullen.d.faustino@gmail.com', '$2y$10$P5eSYMMsB01HlmS0xysAguU5NQx9d2SQaK7WohLhxl1JPXoosOOuW', 'S3RTaVU0eWZnV0Fs', 1234, 'c', 'default-avatar.png', NULL, NULL, NULL, '{"member":1}', 2, 0, 'NOeDFqgKScbovpL1lfXDoMEoGBex95hjj1PRnBJmU2', NULL, NULL, NULL, NULL, 1, NULL, 1, '2017-05-31 11:13:29', '2017-05-31 11:13:30'),
(13, 'uVsu14962292451IgD', 'd', 'ullen.d.faustino@gmail.com', '$2y$10$Nu7PAgtXHv82IdXFyhkOCO.OvQMIraNVh6yaFGZ5eo2Noe0.URJqW', 'UkRLVnhVd3htcnNU', 1234, 'd', 'default-avatar.png', NULL, NULL, NULL, '{"member":1}', 2, 0, 'BReSqFFF3SCoJQp1IfSoNk7wIcLGrWXGb1yoDMHDWQ', NULL, NULL, NULL, NULL, 1, NULL, 1, '2017-05-31 11:14:05', '2017-05-31 11:14:05'),
(14, 'Xmuu14962292869Gpr', 'e', 'ullen.d.faustino@gmail.com', '$2y$10$MzydJoBNKNw7nvH3Stkz9.HcI4OjFxNyNRsUmfGc9KhuYVuHig/om', 'NHB4VXJ4Mm5JRHpW', 1234, 'e', 'default-avatar.png', NULL, NULL, NULL, '{"member":1}', 2, 0, 'SpUjY1FE8RjLQZd5o0LK9HupfdWloPJnclk6aQhz5D', NULL, NULL, NULL, NULL, 1, NULL, 1, '2017-05-31 11:14:46', '2017-05-31 11:14:47'),
(15, 'RHfM1496229356fZaQ', 'f', 'ullen.d.faustino@gmail.com', '$2y$10$O.1ECM//BmmSQY/BE1u89.EauRN.gkmBDYYT43ALAQKrfSjd3WGJ6', 'dmxJMUwwSGl5YXA4', 1234, 'f', 'default-avatar.png', NULL, NULL, NULL, '{"member":1}', 2, 0, '6Mix9exZtt0augmVXcUh8qsTk3hvhdYgvSYmD92lWf', NULL, NULL, NULL, NULL, 1, NULL, 1, '2017-05-31 11:15:56', '2017-05-31 11:15:57'),
(16, 'AM9F1496229406UACt', 'g', 'ullen.d.faustino@gmail.com', '$2y$10$iJ9kAxmuEsqxvBNQAoUKcuXICR2PvXar9TMZ9pYDYF0e0Gm3dk.QG', 'YUhUTkpJcXB3TktW', 1234, 'g', 'default-avatar.png', NULL, NULL, NULL, '{"member":1}', 2, 0, 'VKFU8B8gIMjIgnNVjjrUzQNbRuY8pg3CLKdkvGK9Pf', NULL, NULL, NULL, NULL, 1, NULL, 1, '2017-05-31 11:16:46', '2017-05-31 11:16:46'),
(17, '1dcK1496229479QdjS', 'h', 'ullen.d.faustino@gmail.com', '$2y$10$4pQ8W9Adn9RWc73SFrBVM.KN5Bl4uasje2ilME40DJm39RSBsft6O', 'RWdCamk5ZXZNUzh1', 1234, 'h', 'default-avatar.png', NULL, NULL, NULL, '{"member":1}', 2, 0, 'RgGDy6oANHrBK2RF2YTjfHF0D95IIamaOR8QlyoUgo', NULL, NULL, NULL, NULL, 1, NULL, 1, '2017-05-31 11:17:59', '2017-05-31 11:18:00'),
(18, 'lITj1496229519WETf', 'i', 'ullen.d.faustino@gmail.com', '$2y$10$AbG7RT8vjyWgxaKsrIeameWwFNTpJ.hjZb5FEV2ItpXCnZ3b4jvx6', 'VVVYUXpWR0Q2Y3Iw', 1234, 'i', 'default-avatar.png', NULL, NULL, NULL, '{"member":1}', 2, 0, 'dsYTq7IaIYgcWr8ffNULan4rIlTCr9ARzdzUh00YIH', NULL, NULL, NULL, NULL, 1, NULL, 1, '2017-05-31 11:18:39', '2017-05-31 11:18:40'),
(19, 'NgNJ1496229548TyYG', 'j', 'ullen.d.faustino@gmail.com', '$2y$10$72.ujMwAF4fmPc.J4oYXC.m4W5nI65K4G9w9jOi7Ajf5MNbA2Txgi', 'MzhZVzFGVEhtRDdo', 1234, 'j', 'default-avatar.png', NULL, NULL, NULL, '{"member":1}', 2, 0, 'k8XBgA84fKNEZrdaWr27SBo1o9wJ4CJFdrUEDafyAB', NULL, NULL, NULL, NULL, 1, NULL, 1, '2017-05-31 11:19:09', '2017-05-31 11:19:09'),
(20, 'M7qb1496229604LB5Q', 'k', 'ullen.d.faustino@gmail.com', '$2y$10$biqTG0IQhMOIjZBSoNxyA.Ebx12k80iwWxEBaZbovlH20GbVKXhXu', 'RUVOaFpqWExiaE5D', 1234, 'k', 'default-avatar.png', NULL, NULL, NULL, '{"member":1}', 2, 0, 'd7Udo06zR1K97LRmMADNponkEvhZotq01uawllTXhT', NULL, NULL, NULL, NULL, 1, NULL, 1, '2017-05-31 11:20:04', '2017-05-31 11:20:04');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users_has_hierarchy_siblings`
--

CREATE TABLE `users_has_hierarchy_siblings` (
  `user_id` int(11) NOT NULL,
  `hierarchy_sibling_id` int(11) NOT NULL,
  `table` int(8) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_has_hierarchy_siblings`
--

INSERT INTO `users_has_hierarchy_siblings` (`user_id`, `hierarchy_sibling_id`, `table`) VALUES
(6, 1, 1),
(7, 2, 1),
(7, 3, 1),
(8, 4, 1),
(8, 5, 1),
(9, 6, 1),
(9, 7, 1),
(10, 8, 1),
(10, 9, 1),
(11, 10, 1),
(11, 11, 1),
(12, 12, 1),
(12, 13, 1),
(13, 14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_balance`
--

CREATE TABLE `user_balance` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `current_balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_balance` decimal(10,2) DEFAULT '0.00',
  `table` int(1) DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_control`
--
ALTER TABLE `admin_control`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bulletin`
--
ALTER TABLE `bulletin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `codes`
--
ALTER TABLE `codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `constants`
--
ALTER TABLE `constants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `direct_referrals`
--
ALTER TABLE `direct_referrals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exit_history`
--
ALTER TABLE `exit_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hierarchy_siblings`
--
ALTER TABLE `hierarchy_siblings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_has_users_users4_idx` (`recruitee_id`);

--
-- Indexes for table `master_password`
--
ALTER TABLE `master_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_payout_requests`
--
ALTER TABLE `member_payout_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_code`
--
ALTER TABLE `purchase_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_code_proof`
--
ALTER TABLE `purchase_code_proof`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD KEY `fk_throttle_users_idx` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `fk_users_group_groups1_idx` (`group_id`);

--
-- Indexes for table `users_has_hierarchy_siblings`
--
ALTER TABLE `users_has_hierarchy_siblings`
  ADD PRIMARY KEY (`user_id`,`hierarchy_sibling_id`),
  ADD KEY `fk_users_has_hierarchy_children_hierarchy_children1_idx` (`hierarchy_sibling_id`),
  ADD KEY `fk_users_has_hierarchy_children_users1_idx` (`user_id`);

--
-- Indexes for table `user_balance`
--
ALTER TABLE `user_balance`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_control`
--
ALTER TABLE `admin_control`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `bulletin`
--
ALTER TABLE `bulletin`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `codes`
--
ALTER TABLE `codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `constants`
--
ALTER TABLE `constants`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `direct_referrals`
--
ALTER TABLE `direct_referrals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `exit_history`
--
ALTER TABLE `exit_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hierarchy_siblings`
--
ALTER TABLE `hierarchy_siblings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `master_password`
--
ALTER TABLE `master_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `member_payout_requests`
--
ALTER TABLE `member_payout_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase_code`
--
ALTER TABLE `purchase_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `purchase_code_proof`
--
ALTER TABLE `purchase_code_proof`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `user_balance`
--
ALTER TABLE `user_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `hierarchy_siblings`
--
ALTER TABLE `hierarchy_siblings`
  ADD CONSTRAINT `fk_users_has_users_users4` FOREIGN KEY (`recruitee_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `throttle`
--
ALTER TABLE `throttle`
  ADD CONSTRAINT `fk_throttle_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_group_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_group_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_has_hierarchy_siblings`
--
ALTER TABLE `users_has_hierarchy_siblings`
  ADD CONSTRAINT `fk_users_has_hierarchy_children_hierarchy_children1` FOREIGN KEY (`hierarchy_sibling_id`) REFERENCES `hierarchy_siblings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_has_hierarchy_children_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
